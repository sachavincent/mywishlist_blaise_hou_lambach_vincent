<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../web/assets/images/favicon.png" type="image/png">


    <link href="../web/assets/plugins/morris-chart/morris.css" rel="stylesheet">
    <link href="../web/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>

    <link href="../web/assets/css/icons.css" rel="stylesheet">
    <link href="../web/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../web/assets/css/style.css" rel="stylesheet">
    <link href="../web/assets/css/responsive.css" rel="stylesheet">

</head>

<body class="sticky-header">


<!--Start login Section-->
<section class="login-section">
    <div class="container">
        <div class="row">
            <div class="login-wrapper">
                <div class="login-inner">

                    <div class="logo">
                        <a href="
                        <?php
                            echo \Slim\Slim::getInstance()->urlFor("accueil")
                        ?>
                            ">
                        <img src="../web/assets/images/log.png"  alt="logo" style="background-color: #444444"/>
                        </a>
                    </div>
                    <br>
                    <?php
                        echo $_SESSION['login'];
                    ?>

                    <div class="copy-text">
                        <p class="m-0">2019 &copy; </p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!--End login Section-->


</body>

</html>
