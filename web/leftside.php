
<div class="logo">
    <a href="
    <?php
    echo \Slim\Slim::getInstance()->urlFor('accueil');
    ?>
    "><img src="../web/assets/images/log.png" alt=""></a>
</div>


<div class="left-side-inner">
    <!--Sidebar nav-->
    <ul class="nav nav-pills nav-stacked custom-nav">
        <li class="menu-list nav-active"><a href="
        <?php
            echo \Slim\Slim::getInstance()->urlFor('accueil');
            ?>
        "><span>Accueil</span></a>

        </li>

        <?php
        $url = \Slim\Slim::getInstance()->urlFor('votreliste');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Vos Listes</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>

        <?php
        $url = \Slim\Slim::getInstance()->urlFor('createur');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Créateurs</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>

        <?php
        $url = \Slim\Slim::getInstance()->urlFor('participation');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Participation</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>

        <?php
        $url=\Slim\Slim::getInstance()->urlFor('uploader');
        $modifier=<<<END
    <li class="menu-list"><a href="$url"> <span>Uploader une image</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])){
            echo $modifier;
        }
        ?>



        <?php
        $url = \Slim\Slim::getInstance()->urlFor('mdp_post');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Modifier le mot de passe</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>
        <li class="menu-list"><a href="
        <?php
            if (isset($_COOKIE["iduser"])) {
                echo \Slim\Slim::getInstance()->urlFor('logout');
            } else {
                echo \Slim\Slim::getInstance()->urlFor('login');
            }

            ?>
        "> <span>
                    <?php
                    if (isset($_COOKIE["iduser"])) {
                        echo "Se déconnecter";
                    } else {
                        echo "Se connecter";
                    }
                    ?></span></a>

        </li>

        <?php
        $url = \Slim\Slim::getInstance()->urlFor('supprimer');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Supprimer votre compte</span></a> </li>
END;
        if (isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>

        <?php
        $url = \Slim\Slim::getInstance()->urlFor('reg_post');
        $modifier = <<<END
    <li class="menu-list"><a href="$url"> <span>Créer un compte</span></a> </li>
END;
        if (!isset($_COOKIE["iduser"])) {
            echo $modifier;
        }
        ?>

    </ul>
    <!--End sidebar nav-->

</div>
    