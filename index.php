<?php

require 'vendor/autoload.php';


use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\models\Reservation;

use mywishlist\vue\VueNouvelleListe;

use mywishlist\controller\ListeController;
use mywishlist\controller\ItemController;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/*
$listl = \mywishlist\models\Liste::all() ;
$vue = new \mywishlist\vue\VueParticipant( $listl->toArray() ) ;
$vue-->render(1);
// pour afficher 1 item
$item = \mywishlist\models\Item::find( $id ) ;
$vue = new \mywishlist\vue\VueParticipant( [ $item ] ) ;
$vue->render( 3 ) ;
*/
session_start();


$app = new \Slim\Slim();
$app->get('/', function () {
    $_SESSION['mode_item'] = "";
    $_SESSION['page'] = 'index';
    \mywishlist\controller\IndexController::afficher();
})->name("accueil");

$app->post('/', function () {
    $_SESSION['page'] = 'index';
    \mywishlist\controller\IndexController::afficher();
})->name("accueil_post");

$app->get('/votre_liste', function () {
    $_SESSION['mode_item'] = "";
    $_SESSION['page'] = 'index';
    \mywishlist\controller\VotreListeController::afficher();
})->name("votreliste");

$app->post('/vos_listes', function () {
    $_SESSION['mode_item']="";

    $_SESSION['page'] = 'index';
    \mywishlist\controller\VotreListeController::afficher();
})->name("votreliste_post");

$app->get('/newlist', function () {
    $_SESSION['page'] = 'newlist';
    \mywishlist\controller\NouvelleListeController::afficher();
    $vue = new VueNouvelleListe();
});

$app->post('/newlist', function () {
    $_SESSION['page'] = 'newlist';
    \mywishlist\controller\NouvelleListeController::afficher();
    $vue = new VueNouvelleListe();
})->name("newlist");

$app->get('/newlist/share', function () {
    $_SESSION['page'] = 'newlistShare';
    $_SESSION["showToken"] = true;
    \mywishlist\controller\NouvelleListeController::afficher();
    $vue = new VueNouvelleListe();
})->name("share");

$app->get('/newlist/edit', function () {
    $_SESSION['page'] = 'newlistEdit';
    $_SESSION["showTokenModif"] = true;
    \mywishlist\controller\NouvelleListeController::afficher();
    $vue = new VueNouvelleListe();
})->name("edit");

$app->get('/liste/:n', function ($n) {
    $liste = Liste::where('token', '=', $n)->first();
    if ($liste != null) {
        $_SESSION['page'] = 'liste';
        $_SESSION['numero_liste'] = $liste->no;
        $_SESSION['liste'] = $liste;
        ListeController::afficher($liste);
    } else {
        $liste = Liste::where('no', '=', $n)->first();
        if ($liste != null && $liste->publique === 1) {
            $_SESSION['page'] = 'liste';
            $_SESSION['numero_liste'] = $liste->no;
            $_SESSION['liste'] = $liste;
            ListeController::afficher($liste);
        } else {
            $liste = Liste::where('tokenModif', '=', $n)->first();
            if ($liste != null) {
                $_SESSION['page'] = 'listeModif';
                $_SESSION['numero_liste'] = $liste->no;
                $_SESSION['liste'] = $liste;
                ListeController::afficher($liste);
            }
        }
    }
});

$app->post('/liste/:n', function ($n) {
    $liste = Liste::where('token', '=', $n)->first();
    if ($liste != null) {
        $_SESSION['page'] = 'liste';
        $_SESSION['numero_liste'] = $liste->no;
        $_SESSION['liste'] = $liste;
        ListeController::afficher($liste);
    } else {
        $liste = Liste::where('no', '=', $n)->first();
        if ($liste != null && $liste->publique === 1) {
            $_SESSION['page'] = 'liste';
            $_SESSION['numero_liste'] = $liste->no;
            $_SESSION['liste'] = $liste;
            ListeController::afficher($liste);
        } else {
            $liste = Liste::where('tokenModif', '=', $n)->first();
            if ($liste != null) {
                $_SESSION['page'] = 'listeModif';
                $_SESSION['numero_liste'] = $liste->no;
                $_SESSION['liste'] = $liste;
                ListeController::afficher($liste);
            }
        }
    }
})->name('liste');

$app->get('/item/:id', function ($id) {
    if ($_SESSION['page'] !== 'item')
        $_SESSION['prev_page'] = $_SESSION['page'];

    $_SESSION['page'] = 'item';
    $item = Item::where('id', '=', $id)->first();
    $_SESSION['item'] = $item;
    if ($item->liste_id != null)
        ItemController::afficher($item);
});

$app->post('/item/:id', function ($id) {
    if ($_SESSION['page'] !== 'item')
        $_SESSION['prev_page'] = $_SESSION['page'];

    $_SESSION['page'] = 'item';
    $item = Item::where('id', '=', $id)->first();
    $_SESSION['item'] = $item;
    ItemController::afficher($item);
})->name('items');

$app->get('/createur', function () {

    $_SESSION['page'] = 'createur';
    \mywishlist\controller\CreateurController::afficher();
})->name('createur');

$app->get('/participation', function () {

    $_SESSION['page'] = 'participation';
    \mywishlist\controller\ParticipationController::afficher();
})->name('participation');


$app->get('/uploader', function () {

    $_SESSION['page'] = 'uploader';
    \mywishlist\controller\UploaderController::afficher();

})->name('uploader');

$app->post('/uploader', function () {

    $_SESSION['page'] = 'uploader';
    \mywishlist\controller\UploaderController::afficher();

})->name('uploader_post');

$app->get('/editlist/:token', function ($token) {
    $l = Liste::where('token', '=', $token)->first();
    $_SESSION['liste'] = $l;

    //$_SESSION['contenu']=$_POST['val-username'];

    \mywishlist\controller\EditionListeController::afficher($l);
});

$app->get('/login', function () {

    $_SESSION['page'] = 'login';
    \mywishlist\controller\LoginController::afficher();
})->name('login');
$app->post('/login', function () {

    $_SESSION['page'] = 'login';
    \mywishlist\controller\LoginController::afficher();
})->name('login_post');

$app->get('/logout', function () {
    setcookie("iduser", "", time() - 100);
    setcookie("participation", "", time() - 100);
    \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
})->name('logout');

$app->get('/signup', function () {
    $_SESSION['page'] = 'signup';
    \mywishlist\controller\LoginController::afficher();
})->name('reg_post');

$app->get('/modifiermdp', function () {

    $_SESSION['page'] = 'modifiermdp';
    \mywishlist\controller\LoginController::afficher();
})->name('mdp_post');

$app->get('/supprimer', function () {

    $_SESSION['page'] = 'supprimer';
    \mywishlist\controller\CompteController::supprimer();
})->name('supprimer');
/*
$app->get('/listes', function () {
    $app = \Slim\Slim::getInstance();
    echo "affchier mes listes ";
    $listes = m\Liste::get();
    foreach($listes as $liste){
        $url=$app->urlFor('liste des items',['no'=>$liste->no]);
        echo <<<END
            <a href="$url">
             <br>{$listes->titre} ({$liste->no})
            </a>
END;

     }

});
*/
$app->run();

