<?php

namespace mywishlist\vue;

class VueNouvelleListe extends Vue {
    function __construct() {
        parent::__construct();
        if (!isset($_SESSION["contenu"])) {
            $_SESSION["contenu"] = "";
        }
    }

    function render() {
        parent::render();
    }

    function ajout() {
        $url = \Slim\Slim::getInstance()->urlFor("newlist");
        $_SESSION["contenu"] = "
            <div class='header-title'>Nouvelle Liste</div>
            <form class=\"form-horizontal\" method=\"POST\" action=$url>
                             <div class=\"form-group\">
                            <label class=\"col-md-2 control-label\">Titre</label>
                            <div class=\"col-md-10\">
                              <input class=\"form-control\" name=\"title\" type=\"text\">
                            </div>
                             </div>
                             <div class=\"form-group\">
                          <label class=\"col-md-2 control-label\">Description</label>
                            <div class=\"col-md-10\">
                              <input class=\"form-control\" name=\"desc\" type=\"text\">
                            </div>
                            </div>
                           <div class=\"form-group\">
                          <label class=\"col-md-2 control-label\">Date d'expiration</label>
                            <div class=\"col-md-10\">
                              <input class=\"form-control\" name=\"exp\" type=\"date\">
                            </div>
                            </div>
                            <div class=\"form-group\">
                            <div class=\"col-md-8 col-md-offset-3\">
                              <button class=\"btn  btn-primary\" name=\"submit\" type=\"submit\">Valider</button>
                            </div>
                           </div>
                 </form>";

        if (isset($_SESSION["createdList"])) {
            $_SESSION["contenu"] .= "</br></br></br>
        <div>
            <div class=\"form-group\">
                <div class=\"col-md-8 col-md-offset-3\">
                    <a href=" . \Slim\Slim::getInstance()->urlFor("share") . "><button class=\"btn  btn-primary\" name='shareButton' type='submit'>Partager</button></a>
                </div>
            </div>
        </div>";

            if (isset($_SESSION["showToken"])) {
                $token = $_SESSION["createdList"]->token;
                $_SESSION["contenu"] .= "<a type='text' href=/www/blaise84u/liste/" . $token . " name='shareLink'>Cliquez-ici. </a>";
            }

            $_SESSION["contenu"] .= "</br></br>";

            $_SESSION["contenu"] .= "
            <div>
            <div class=\"form-group\">
                <div class=\"col-md-8 col-md-offset-3\">
                    <a href=" . \Slim\Slim::getInstance()->urlFor("edit") . "><button class=\"btn  btn-primary\" name='editButton' type='submit'>Modifier</button></a>
                </div>
            </div>
        </div>";

            if (isset($_SESSION["showTokenModif"])) {
                $tokenModif = $_SESSION["createdList"]->tokenModif;
                $_SESSION["contenu"] .= "<a type='text' href=/www/blaise84u/liste/" . $tokenModif . " name='shareLink'>Cliquez-ici. </a>";
            }
        }
    }
}