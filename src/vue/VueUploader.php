<?php

namespace mywishlist\vue;

class VueUploader extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {
        parent::render();
    }

    function getUpload() {
        $url = \Slim\Slim::getInstance()->urlFor("uploader_post");
        $_SESSION["contenu"] = <<<END
<div class="row">
                        <div class="col-md-12">
                            <form action="$url" class="dropzone" id="dropzone" enctype="multipart/form-data" method="post">
                           
                                <input name="file" type="file" multiple />                            
                                    <br>
                                 <input name="submit" value="uploader" type="submit" multiple />
                            </form>
                        </div>
                    </div>
END;

    }

    function getMessage($message) {
        $_SESSION["contenu"] .= <<<END
        <div class="row">
            <h1>$message</h1>
        </div>
END;

    }
}