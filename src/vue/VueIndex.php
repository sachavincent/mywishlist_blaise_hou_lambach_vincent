<?php

namespace mywishlist\vue;

use mywishlist\models\Liste;

class VueIndex extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {

        parent::render();
    }

    function getindex() {
        $url = \Slim\Slim::getInstance()->urlFor("accueil_post");
        $urlcreation = \Slim\Slim::getInstance()->urlFor("newlist");

        if (isset($_SESSION["createdList"]))
            unset($_SESSION["createdList"]);
        if (isset($_SESSION["showToken"]))
            unset($_SESSION["showToken"]);
        if (isset($_SESSION["showTokenModif"]))
        unset($_SESSION["showTokenModif"]);

        $_SESSION["contenu"] = <<<END
        
                     <div class="row">
                
                      <div class="col-md-12">
                 <div class="white-box">
                    <h2 class="header-title">Listes publiques</h2>
                     <div class="table-responsive">
                         <table class="table table-bordered">
                          <thead>
                            <tr>
                            
                              <th>Numéro</th>
                              <th>Titre</th>
                              <th>Expiration</th>
                            </tr>
                          </thead>
                          <tbody>
END;
        $num=1;

        $listes=Liste::where('publique','=','1')->orderBy('expiration','ASC')->get();
        foreach ($listes as $liste){
            $titre=$liste['titre'];
            $url = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste['no']]);
            if (date('Y-m-d', strtotime($liste['expiration'])) > date('Y-m-d')) {
                $date=$liste['expiration'];
                $_SESSION["contenu"].=<<<END
            <tr>
                              <td>$liste->no</td>
                              <td><a href=$url>$titre</a> </td>
                              <td>$date</td>
                            </tr>           
END;
                        }
   } 



$_SESSION["contenu"].=<<<END
            </tbody>
                        </table>
    
                     </div>
                 </div>
                 </div>
                 
                     <div class="col-md-6">
                      <div class="white-box">
                         <h2 class="header-title">Voulez-vous créer votre liste ?</h2>
                         <div class="button-wrap">
                            <a href=$urlcreation><button type="button" class="btn btn-primary btn-block" >Créer une liste</button></a>
                        </div>
                      </div>
                   </div>

              </div>
END;




    }
}