<?php

namespace mywishlist\vue;


use mywishlist\models\Cagnotte;
use mywishlist\models\Liste;

use mywishlist\models\Reservation;

class VueItem extends Vue {
    function __construct() {
        parent::__construct();
        if (!isset($_SESSION["contenu"])) {
            $_SESSION["contenu"] = "";
        }
        if (!isset($_SESSION['mode_item'])) {
            $_SESSION['mode_item'] = "";
        }

    }

    function render() {
        parent::render();
    }

    function ajout($item) {
        $liste = Liste::where("no", "=", $item->liste_id)->first();
        $_SESSION['liste'] = $liste;
        $url = \Slim\Slim::getInstance()->urlFor('items', ['id' => $item->id]);
        $reservation = Reservation::where('iditem', '=', $item->id)->first();
        $cagnotte = Cagnotte::where('iditem', '=', $item->id)->first();
        $_SESSION['reservation'] = $reservation;
        $_SESSION["cagnotte"] = $cagnotte;

        if (isset($_SESSION['prev_page'])) {
            $urlListe = $url;
            if ($_SESSION['prev_page'] == 'liste') {
                if ($liste->publique === 1)
                    $urlListe = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->no]);
                else if ($liste->publique === 0)
                    $urlListe = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->token]);
            } else if ($_SESSION['prev_page'] === 'listeModif')
                $urlListe = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->tokenModif]);

            if ($urlListe !== $url) {
                $_SESSION["contenu"] = '
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-0">
                              <img alt="" src="/www/blaise84u/web/assets/images/back_icon.png" width="20px">
                              <a href="' . $urlListe . '"><button class="btn btn-link" name="back" type="button">Liste</button></a>
                            </div>
                          </div>';
            } else
                $_SESSION["contenu"] = '';
        } else
            $_SESSION["contenu"] = '';

        $_SESSION["contenu"] .= '
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">';
        $this->getInformation($item);
        if ((isset($_COOKIE["iduser"]) && $_COOKIE["iduser"] == $liste->user_id)) {
            if ($_SESSION['mode_item'] != 'modifier') {

                $_SESSION["contenu"] .= '<dl><dt>Réservation</dt>';
                if ($reservation != null)
                    $_SESSION["contenu"] .= '<dd>Réservé</dd>';
                else
                    $_SESSION["contenu"] .= '<dd>Aucune réservation</dd>';

                if ($reservation == null) {
                    $_SESSION["contenu"] .= <<<END
                 <form class="js-validation-bootstrap form-horizontal" action=$url method="post">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" name="modifier" value="modifier" type="submit">Modifier</button>
                            </div>
                          </div>
                 </form>
                   
END;
                }
            } else {
                $_SESSION["contenu"] .= <<<END
                <form class="js-validation-bootstrap form-horizontal" action=$url method="post">
                        <div class="form-group">
                            <div class="col-md-8">
                                <img alt="" src="/www/blaise84u/web/assets/images/delete_icon.png" width="20px"/>
                                <button class="btn  btn-primary" name="supprimer" value="Supprimer" type="submit">Supprimer</button>
                            </div>
                        </div>
                 </form>
END;
                $this->getModifier($item);
            }


            if (Cagnotte::where('iditem', '=', $item->id)->first() == null) {
                $_SESSION["contenu"] .= <<<END
                 <form class="js-validation-bootstrap form-horizontal" action=$url method="post">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" name="creerCagnotte" value="creerCagnotte" type="submit">Créer une cagnotte</button>
                            </div>
                        </div>
                 </form>
END;
            }
            $_SESSION["contenu"] .= '</dl></div></div></div>';
        } else if ($cagnotte != null && $cagnotte->first()->tarifRestant > 0)
            $this->getCagnotte();

    }

    function getInformation($item) {

        $image = "/www/blaise84u/images/" . $item->img;
        if ($item->url != '')
            $image = $item->url;
        $_SESSION["contenu"] .= '
            <h2 class="header-title" > Item</h2 >
                              <dl>
                                <dd><img alt = "" src ="' . $image . '" class="profile-photo" /></dd >
                                <dt> Numéro d\'item</dt>
                                <dd>' . $item->id . '</dd>
                                <dt>Nom d\'item</dt>
                                <dd>' . $item->nom . '</dd>
                                <dt>Description  d\'item</dt>
                                <dd>' . $item->descr . '</dd>
                                <dt>Tarif d\'item</dt>
                                <dd>' . $item->tarif . '&euro;</dd>';
        if ($_SESSION["cagnotte"] != null) {
            if ($_SESSION["cagnotte"]->tarifRestant > 0)
                $_SESSION["contenu"] .= '<dd>Cagnotte en cours : ' . $_SESSION["cagnotte"]->tarifRestant . '&euro; restants.</dd>';
            else
                $_SESSION["contenu"] .= '<dd>Cagnotte terminée : Item financé.</dd>';
        }
    }


    function getModifier($item) {
        $url = \Slim\Slim::getInstance()->urlFor('items', ['id' => $item->id]);

        $image = "/www/blaise84u/images/" . $item->img;
        if ($item->url != '')
            $image = $item->url;

        $_SESSION["contenu"] .= <<<END
                            <form class="form-horizontal" action=$url method="post">
                        
                          <div class="form-group">
                            <label class="col-md-2 control-label">Image locale</label>
                                  <input class="form-control" value="locale" name="imageType"
END;
        if ($item->url == '')
            $_SESSION["contenu"] .= ' checked="checked";';
        $_SESSION["contenu"] .= <<<END
                            id="localRadio" type="radio" style="height:20px; width:20px; !important;">
                            <div class="col-md-10">
                                  <input class="form-control" name="itemImage" type="file" value="$item->img" formenctype="multipart/form-data" accept=".png, .jpg, .jpeg, .gif">
                            </div>
                             </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">URL Image</label>
                                  <input class="form-control" value="url" name="imageType"
END;
        if ($item->url != '')
            $_SESSION["contenu"] .= ' checked="checked";';
        $_SESSION["contenu"] .= <<<END
                            id="urlRadio" type="radio" style="height:20px; width:20px; !important;">
                            <div class="col-md-10">
                                  <input class="form-control" name="itemURL" type="url" value="$item->url">
                            </div>
                             </div>
                             
                          <div class="form-group">
                            <label class="col-md-2 control-label">Supprimer l'image</label>
                                  <input class="form-control" value="deleteImage" name="imageType" id="urlRadio" type="radio" style="height:20px; width:20px; !important;">
                             </div>
                             
                          <div class="form-group">
                            <label class="col-md-2 control-label">Nom</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_nom" type="text" value="$item->nom">
                            </div>
                             </div>
                             <div class="form-group">
                          <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_descr" type="text" value="$item->descr">
                            </div>
                            </div>
                           <div class="form-group">
                          <label class="col-md-2 control-label">Tarif</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_tarif" type="text" value="$item->tarif">
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" name="valider" type="submit">Valider</button>
                            </div>
                           </div>
                     
                 </form>
END;

    }


    static function getReservation() {
        $url = \Slim\Slim::getInstance()->urlFor('items', ['id' => $_SESSION['item']->id]);
        if (!isset($_COOKIE["iduser"]) || (isset($_SESSION["liste"]) && $_COOKIE["iduser"] != $_SESSION["liste"]->user_id)) {
            $contenu = <<<END
            </div> 
        <div class="row">
        <div class="white-box">  
                       <h2 class="header-title">Réservation</h2>
                         <form class="js-validation-bootstrap form-horizontal" action=$url method="post">
                          <div class="form-group">
                            <label class="col-md-3 control-label" for="val-username">Nom<span class="text-danger">*</span></label>
                            <div class="col-md-9">
                              <input class="form-control" type="text" id="val-username" name="val-username"
END;


            if (isset($_COOKIE["participation"]) && $_COOKIE["participation"] != null) {
                $idpar = $_COOKIE["participation"];
                $contenu .= <<<END
            readonly="true" value="$idpar"
END;

            } else {
                $contenu .= "placeholder=\"Entrez votre nom\"";
            }
            $contenu .= <<<END
                >
                            </div>
                            </br>
                            </br>
                            </br>
                            <label class="col-md-3 control-label" for="val-message">Message</label>
                            <div class="col-md-9">
                              <input class="form-control" type="text" id="val-message" name="val-message" placeholder="Entrez votre message">
                            </div>
                            </br>
                            </br>
                            </br>
                          <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" type="submit">Valider</button>
                            </div>
                          </div>
                        </form>
                       </div>
                       </div>
</div>
END;
            return $contenu;
        }
        return "";
    }

    static function getCagnotte() {
        $url = \Slim\Slim::getInstance()->urlFor('items', ['id' => $_SESSION['item']->id]);
        if (isset($_SESSION["cagnotte"])) {
            $cagnotte = $_SESSION["cagnotte"];
            if (!isset($_COOKIE["iduser"]) || (isset($_SESSION["liste"]) && $_COOKIE["iduser"] != $_SESSION["liste"]->user_id)) {
                $contenu = <<<END
        <div class="row">
            <div class="white-box">  
                <h2 class="header-title">Cagnotte</h2>
                <form class="js-validation-bootstrap form-horizontal" action=$url method="post">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="valeurCagnotte">Contribution (de 0.01 à $cagnotte->tarifRestant&euro;) :<span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" type="number" step=0.01 min=0.01 max=$cagnotte->tarifRestant name="valeurCagnotte" name="valeurCagnotte">
                        </div>
                        </br>
                        </br>
                        </br>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                                <button class="btn  btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
END;
                return $contenu;
            }
        }
        return "";
    }
}