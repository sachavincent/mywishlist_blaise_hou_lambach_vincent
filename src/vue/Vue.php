<?php

namespace mywishlist\vue;


class Vue {
    function __construct() {

    }

    public function ajouter($principale) {
        $_SESSION['acceuilprincipale']= $principale;
    }


    public function render() {
        include('web/index.php');
    }

    public static function getSession() {
        if (isset($_SESSION['acceuilprincipale'])) {
            return $_SESSION['acceuilprincipale'];
        } else {
            return "rien merci de contacter l'administrateur du site";
        }
    }

}