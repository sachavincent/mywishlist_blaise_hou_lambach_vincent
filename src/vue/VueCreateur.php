<?php

namespace mywishlist\vue;

use mywishlist\models\Liste;
use mywishlist\models\User;

class VueCreateur extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {
        parent::render();
    }

    function getCreateur() {

        $_SESSION["contenu"] = <<<END
                <div class="row">
             
               
                <div class="col-md-6">
                 <div class="white-box">
                    <h2 class="header-title">Liste de créateur</h2>
                     <div class="table-wrap">
                            <table class="table">
                          <thead>
                            <tr>
                              <th>Créateur</th>
                            </tr>
                          </thead>
                          
                          <tbody>
END;
        $list = Liste::select('user_id')->where('publique', '=', 1)->get();
        foreach ($list as $user) {

            $name = User::select('username')->where('iduser', '=', $user)->first()['username'];

            $_SESSION['contenu'] .= <<<END
             <tr>
             
                              <td>$name</td>
                            </tr>
END;

        }

        $_SESSION["contenu"] .= <<<END
                          </tbody>
                        </table>
                     </div>
                 </div>
                 </div>
    </div>
END;
    }
}