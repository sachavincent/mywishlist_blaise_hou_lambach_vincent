<?php

namespace mywishlist\vue;

use mywishlist\models\Liste;

class VueVotreListe extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {
        parent::render();
    }

    function getListe() {
        $url = \Slim\Slim::getInstance()->urlFor("votreliste_post");
        $urlcreation = \Slim\Slim::getInstance()->urlFor("newlist");

        $_SESSION["contenu"] = <<<END
                     <div class="row">
                <div class="col-md-12">
                 <div class="white-box">
                     <h2 class="header-title">Vos listes</h2>
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Numéro de liste</label>
                        <div class="col-sm-5">
                        <form action=$url method=POST>
                         <div class="form-group">
                        <select class="form-control m-b-15" name="numero_liste">
END;
        if (isset($_COOKIE["iduser"])) {
            $listes = Liste::where("user_id", "=", $_COOKIE['iduser'])->get();
            foreach ($listes as $liste) {
                $_SESSION["contenu"] .= "<option>" . $liste->no . "</option>";
            }
        }
        $_SESSION["contenu"] .= <<<END
                        
                        </select>
                        
                              
                                <div class="col-md-8 col-md-offset-3">
                                  <button class="btn  btn-primary" type="submit">Choisir</button>
                                </div>
                              </div>
                           </form>
                        </div>
                      </div>
                        
                        </form>
                       </div>
                   </div>
                   <div class="clear"></div>
                      
                      
                     <div class="col-md-6">
                      <div class="white-box">
                         <h2 class="header-title">Voulez-vous créer votre liste ?</h2>
                         <div class="button-wrap">
                            <a href=$urlcreation><button type="button" class="btn btn-primary btn-block" >Créer une liste</button></a>
                        </div>
                      </div>
                   </div>

              </div>
END;

    }
}