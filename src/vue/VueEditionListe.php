<?php

namespace mywishlist\vue;

class VueEditionListe extends Vue {
    function __construct() {
        parent::__construct();
        if (!isset($_SESSION["contenu"])) {
            $_SESSION["contenu"] = "";
        }
    }

    function render() {
        parent::render();
    }

    function modif($titre, $desc, $exp, $token) {
        $url = \Slim\Slim::getInstance()->urlFor("editlist/$token");
        $_SESSION["contenu"] = "
            <div>Nouvelle Liste</div>
            <form method=\"POST\" action=$url>
                <a>Titre</a><input type='text' name='title' value=$titre>
                <a>Description</a><input type='text' name='desc' value=$desc>
                <a>Date d'expiration</a><input type='date' name='exp' value=$exp>
                <input type='submit' value='submit'>
            </form>";

        if (isset($_SESSION["createdList"])) {
            $_SESSION["contenu"] .= "</br></br></br>
        <div>
            <div class=\"form-group\">
                <div class=\"col-md-8 col-md-offset-3\">
                    <a href=" . \Slim\Slim::getInstance()->urlFor("share") . "></a>
                </div>
            </div>
        </div>";
        }
    }
}