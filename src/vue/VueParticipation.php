<?php

namespace mywishlist\vue;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Reservation;
use mywishlist\models\User;

class   VueParticipation extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {
        parent::render();
    }

    function getParticipation() {

        $_SESSION["contenu"] = <<<END
                <div class="row">
             
               
                <div class="col-md-6">
                 <div class="white-box">
                    <h2 class="header-title">Liste de Participation</h2>
                     <div class="table-wrap">
                            <table class="table">
                          <thead>
                            <tr>
                            
                              <th>Numéro</th>
                              <th>Nom d'item</th>
                              <th>Url</th>
                              <th>Tarif</th>
                              <th>Créateur</th>
                              <th>Titre de liste</th>
                              <th>Expiration</th>
                            </tr>
                      
                          </thead>
                          
                          <tbody>
END;

        $reservation = Reservation::where('participant', '=', $_COOKIE['participation'])->get();
        $num = 1;

        foreach ($reservation as $part) {

            $item = Item::where('id', '=', $part->iditem)->first();
            $liste = Liste::where('no', '=', $item['liste_id'])->first();
            $user = User::where('iduser', '=', $liste['user_id'])->first();
            $username = $user['username'];
            $titre = $liste['titre'];
            $expiration = $liste['expiration'];
            $_SESSION['contenu'] .= <<<END
             <tr>
                            <td>$num</td>
                              <td>$item->nom</td>
                              <td>$item->url</td>
                              <td>$item->tarif</td>
                              <td>$username</td>
                              <td>$titre</td>
                              <td>$expiration</td>
                            </tr>
END;
            $num++;
        }

        $_SESSION["contenu"] .= <<<END
                          </tbody>
                        </table>
                     </div>
                 </div>
                 </div>
    </div>
END;
    }
}