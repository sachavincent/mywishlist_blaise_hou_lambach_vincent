<?php

namespace mywishlist\vue;


use mywishlist\models\AssociationListeMessages;
use mywishlist\models\Item;
use mywishlist\models\Message;
use mywishlist\models\Reservation;

class VueListe extends Vue {
    function __construct() {
        parent::__construct();
        if (!isset($_SESSION["contenu"])) {
            $_SESSION["contenu"] = "";
        }
    }

    function render() {
        parent::render();
    }

    function ajout($liste) {
        $urlModif = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->tokenModif]);
        if ($_SERVER['REQUEST_URI'] === $urlModif)
            $url = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->tokenModif]);
        else if ($liste->publique === 0)
            $url = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->token]);
        else if ($liste->publique === 1)
            $url = \Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->no]);
        else
            $url = null;

        $_SESSION["contenu"] = " <div>
            <form class=\"js-validation-bootstrap form-horizontal\" action=$url method=\"post\">
                        <div class=\"form-group\">
                            <div class=\"col-md-8 col-md-offset-3\">
                              <input type=text name=\"ajouter\" placeholder='Ajouter un message'>
                              <button class=\"btn  btn-primary\" name=\"ajouterB\" value=\"ajouterB\" type=\"submit\">Ajouter</button>
                            </div>
                          </div>
                 </form>
            </div>
        ";

        $_SESSION["contenu"] .= "<div class=row><div class=col-md-12><div class=white-box>";
        if ((isset($_COOKIE["iduser"]) && $_COOKIE["iduser"] == $liste->user_id) || $url == $urlModif) {

            $_SESSION["contenu"] .= "
                <div>
                    <form class='js-validation-bootstrap form-horizontal' action=" . $url . " method='post'>
                        <div class='form-group'>
                            <div class='col-md-8 col-md-offset-0'>
                                <button class='btn btn-primary' name='listState' value='listState' type='submit'>Rendre ";
            if ($liste->publique === 0)
                $_SESSION["contenu"] .= "publique";
            else if ($liste->publique === 1)
                $_SESSION["contenu"] .= "privée";
            $_SESSION["contenu"] .= "</button>
                            </div>
                        </div>
                    </form>
                </div>";
            if (isset($_COOKIE["iduser"]) && $liste->user_id == -1) {
                $_SESSION["contenu"] .= "
                <div>
                    <form class='js-validation-bootstrap form-horizontal' action=" . $url . " method='post'>
                        <div class='form-group'>
                            <div class='col-md-8 col-md-offset-2'>
                                <button class='btn btn-primary' name='ownList' value='ownList' type='submit'>Récupérer la liste</button>";
            }
        }
        if ($url === $urlModif) {
            $_SESSION["contenu"] .= "<div>
                    <form class='js-validation-bootstrap form-horizontal' action=" . $url . " method='post'>
                        <div class='form-group'>
                            <div class='col-md-8 col-md-offset-4'>
                                <button class='btn btn-primary' name='addItems' value='addItems' type='submit'>Ajouter des items</button>
                            </div>
                        </div>
                    </form>
                </div>";

            if (isset($_SESSION['addItems'])) {
                $_SESSION["contenu"] .= <<<END
            <form class="form-horizontal" action=$url method="post">
                        
    
                             
                          <div class="form-group">
                            <label class="col-md-2 control-label">Items</label>
                            <div class="col-md-10">
                              <select name="freeItems">
END;
                $_SESSION["contenu"] .= '<option value=-1>Sélectionner un Item libre</option>';
                $items = Item::where("liste_id", "=", "0")->get();
                foreach ($items as $item)
                    $_SESSION["contenu"] .= '<option value=' . $item->id . '>' . $item->nom . '</option>';

                $_SESSION["contenu"] .= <<<END
                
                                
</select>
                            </div>
                             </div>
                             <div class="form-group">
                          <label class="col-md-2 control-label">Nom</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_nom" type="text">
                            </div>
                            </div>
                             <div class="form-group">
                          <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_desc" type="text">
                            </div>
                            </div>
                           <div class="form-group">
                          <label class="col-md-2 control-label">Tarif</label>
                            <div class="col-md-10">
                              <input class="form-control" name="item_tarif" type="number" step="0.01" min="0">
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" name="validerItem" type="submit">Valider</button>
                            </div>
                           </div>
                     
                 </form>
END;
            }
        }
        $_SESSION["contenu"] .= "<h1>
                <id=affichage> La liste numéro : $liste->no
            </h1>
            </br>
            
            <p id=affichage>$liste->titre $liste->description $liste->expiration</p>
            <p id=affichage>";
        $i1 = Item::where('liste_id', '=', $liste->no)->get();

        foreach ($i1 as $item) {
            $_SESSION["contenu"] .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . '-> ' . '<a href=../item/' . $item->id . '>' . $item->nom . '&nbsp;&nbsp;' . '</a>';
            $_SESSION["contenu"] .= '<img alt="" src="/www/blaise84u/images/' . $item->img . '" >';

            $reservation = Reservation::where('iditem', '=', $item->id)->first();

            if (isset($_COOKIE["iduser"]) && $_COOKIE["iduser"] == $liste->user_id) {

                $_SESSION["contenu"] .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                if ($reservation == null)
                    $_SESSION["contenu"] .= 'Non réservé';
                else {
                    $current = strtotime(date("Y-m-d"));
                    $date = strtotime($liste->expiration);

                    $datediff = $date - $current;
                    $difference = floor($datediff / (60 * 60 * 24));

                    if ($difference > 0)
                        $_SESSION["contenu"] .= 'Réservé';
                    else {
                        $_SESSION["contenu"] .= 'Réservé par ' . $reservation->participant;
                        if ($reservation->message == null || $reservation->message === '')
                            $_SESSION["contenu"] .= ' : Aucun message.';
                        else
                            $_SESSION["contenu"] .= ' : ' . $reservation->message;
                    }
                }
            }
            $_SESSION["contenu"] .= '</br></br></br>';
        }
        $_SESSION["contenu"] .= 'Messages publics : ' . '</br></br>';

        $messages = AssociationListeMessages::where("idliste", "=", $liste->no)->get();
        foreach ($messages as $message) {
            $m = Message::where("idmessage", "=", $message->idmessage)->first();
            $_SESSION["contenu"] .= $m->contenu;
            $_SESSION["contenu"] .= '</br>';
            $_SESSION["contenu"] .= '</br>';
        }
        $_SESSION["contenu"] .= "</p>";


        if ($url === $urlModif) {
            $_SESSION["contenu"] .= "<div>
                    <form class='js-validation-bootstrap form-horizontal' action=" . $url . " method='post'>
                        <div class='form-group'>
                            <div class='col-md-8 col-md-offset-4'>
                                <button class='btn btn-primary' name='editList' value='editList' type='submit'>Modifier</button>
                            </div>
                        </div>
                    </form>
                </div>";

            if (isset($_SESSION['editing'])) {
                $_SESSION["contenu"] .= <<<END
            <form class="form-horizontal" action=$url method="post">
                        
    
                             
                          <div class="form-group">
                            <label class="col-md-2 control-label">Titre</label>
                            <div class="col-md-10">
                              <input class="form-control" name="liste_titre" type="text" value="$liste->titre">
                            </div>
                             </div>
                             <div class="form-group">
                          <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                              <input class="form-control" name="liste_description" type="text" value="$liste->description">
                            </div>
                            </div>
                           <div class="form-group">
                          <label class="col-md-2 control-label">Expiration</label>
                            <div class="col-md-10">
                              <input class="form-control" name="liste_expiration" type="date" value="$liste->expiration">
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                              <button class="btn  btn-primary" name="valider" type="submit">Valider</button>
                            </div>
                           </div>
                     
                 </form></div></div></div>
END;
            }
        }

    }

}