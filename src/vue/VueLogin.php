<?php

namespace mywishlist\vue;

use mywishlist\models\User;

class VueLogin extends Vue {
    function __construct() {
        parent::__construct();
    }

    function render() {
        include('web/Login.php');
    }

    function login() {
        $url = \Slim\Slim::getInstance()->urlFor("login_post");
        $urlreg = \Slim\Slim::getInstance()->urlFor("reg_post");
        if (!isset($_SESSION['login_faut'])) {
            $_SESSION['login'] = "";
        } else {
            $_SESSION['login'] = $_SESSION['login_faut'];
        }

        $_SESSION['login'] .= <<<END
<h2 class="header-title text-center">Je me connecte</h2>

                    <form action=$url method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Identifiant" >
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control"  name="password" placeholder="Mot de passe" >
                        </div>


                        <div class="form-group">
                            <input type="submit" value="Valider" class="btn btn-primary btn-block" >
                        </div>

                        <div class="form-group text-center">
                            Première connection?  <a href=$urlreg>Créer un compte </a>
                        </div>

                    </form>
END;

    }

    function geterror() {
        $_SESSION['login_faut'] = <<<END
<p>L'identifiant et/ou le mot de passe sont inconnus.</p>
END;

    }

    function signup() {
        $url = \Slim\Slim::getInstance()->urlFor("login_post");
        $_SESSION['login'] = <<<END
	<h2 class="header-title text-center">Créer mon compte</h2>
                        
                       <form action=$url method="post">
                           
                           <div class="form-group">
                               <input type="text" class="form-control" name="signup_username"  placeholder="Identifiant" >
                           </div>

                           <div class="form-group">
                               <input type="password" class="form-control" name="signup_password" placeholder="Mot de passe" >
                           </div>
						
                          
                           <div class="form-group">
                               <input type="submit" value="Créer" class="btn btn-primary btn-block" >
                           </div>
                           
                           <div class="form-group text-center">
                            J'ai déja un compte <a href=$url>Me connecter </a>
                           </div>
                           
                       </form>
END;

    }

    function mdp(){
        $user=$_COOKIE["iduser"];
        $username=User::where('iduser','=',$user)->first()->username;
        $url = \Slim\Slim::getInstance()->urlFor("login_post");
            $_SESSION['login'] = <<<END
	<h2 class="header-title text-center">Modfier votre mot de passe</h2>        
                       <form action=$url method="post">
                           
                           <div class="form-group">
                                <input type="text" class="form-control" name="modifier_username"  readonly="true" value="$username">
                                
                           </div>

                           <div class="form-group">
                               <input type="password" class="form-control" name="modifier_password" placeholder="Password" >
                           </div>
						
                          
                           <div class="form-group">
                               <input type="submit" value="Modifier" class="btn btn-primary btn-block" >
                           </div>
                           
                           
                           
                       </form> 
END;
    }


}