<?php

namespace mywishlist\controller;

use mywishlist\vue\VueEditionListe as VueEditionListe;


class EditionListeController {

    public static function afficher($l) {
        $affiche = new VueEditionListe();
        $affiche->modif($l->titre, $l->description, $l->expiration, $l->token);
        $affiche->render();
    }

}