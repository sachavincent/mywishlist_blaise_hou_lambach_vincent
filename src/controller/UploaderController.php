<?php

namespace mywishlist\controller;

use mywishlist\vue\VueUploader;

class UploaderController {

    public static function afficher() {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $affiche = new VueUploader();
        $affiche->getUpload();
        if (isset($_FILES["file"])) {
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);
            if ((($_FILES["file"]["type"] == "image/gif")
                    || ($_FILES["file"]["type"] == "image/jpeg")
                    || ($_FILES["file"]["type"] == "image/jpg")
                    || ($_FILES["file"]["type"] == "image/pjpeg")
                    || ($_FILES["file"]["type"] == "image/x-png")
                    || ($_FILES["file"]["type"] == "image/png"))
                && in_array($extension, $allowedExts)) {
                if ($_FILES["file"]["error"] > 0) {
                    $contenu = "Faute ! : " . $_FILES["file"]["error"] . "<br>";
                } else {

                    if (file_exists("images/" . $_FILES["file"]["name"])) {
                        $contenu = $_FILES["file"]["name"] . " existe déjà !";
                    } else {

                        move_uploaded_file($_FILES["file"]["tmp_name"], "images/" . $_FILES["file"]["name"]);
                        $contenu = "upload valide ! ";
                    }
                }
            } else {
                $contenu = "Ce n'est pas une image ! ";
            }
            $affiche->getMessage($contenu);
        }


        $affiche->render();
    }
}