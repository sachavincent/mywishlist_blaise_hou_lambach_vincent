<?php

namespace mywishlist\controller;

use mywishlist\vue\VueItem;
use mywishlist\models\Item;
use mywishlist\models\Cagnotte;
use mywishlist\models\Reservation;

class ItemController {
    /*
     *
     */
    public static function afficher($item) {
        $affiche = new VueItem();
        $r = new Reservation();
        if (isset($_POST['val-username'])) {
            if (Reservation::where('iditem', '=', $_SESSION['item']->id)->first() == null) {
                $r->iditem = $_SESSION['item']->id;
                $r->participant = $_POST['val-username'];

                if (isset($_POST['val-message']))
                    $r->message = $_POST['val-message'];

                $r->save();
            }
        }

        if (isset($_POST['supprimer'])) {
            $it = Item::where("id", "=", $item->id);

            try {
                $it->delete();

                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            } catch (\Exception $e) {
            }
        }


        if (isset($_POST['modifier']))
            $_SESSION['mode_item'] = $_POST['modifier'];

        if (isset($_POST["valider"])) {
            if (isset($_POST["item_nom"]) && isset($_POST["item_tarif"]) && isset($_POST["item_descr"]) && isset($_POST["imageType"])) {
                $it = Item::find($item->id);

                $it->nom = $_POST["item_nom"];
                $it->descr = $_POST["item_descr"];
                $cagnotte = Cagnotte::where('iditem', '=', $item->id)->first();

                if ($cagnotte != null) {
                    $cagnotte->tarifRestant = $cagnotte->tarifRestant + ($_POST["item_tarif"] - $it->tarif);
                    $cagnotte->save();
                }

                $it->tarif = $_POST["item_tarif"];
                $_SESSION["imageType"] = $_POST["imageType"];
                if ($_POST["itemURL"] == '' && $_POST["imageType"] === "url")
                    $_SESSION["imageType"] = "locale";

                if ($_POST["imageType"] === "url" && isset($_POST["itemURL"]) && $_POST["itemURL"] != '') {
                    $it->url = $_POST["itemURL"];
                    $_SESSION["lien_image"] = $_POST["itemURL"];
                } else if (($_POST["imageType"] === "locale" && isset($_POST["itemImage"])) || ($_POST["itemURL"] == '' && $_POST["imageType"] !== "deleteImage")) {
                    if ($_POST["itemImage"] === "")
                        $_POST["itemImage"] = $it->img;
                    $_SESSION["lien_image"] = $_POST["itemImage"];
                    $it->img = $_POST["itemImage"];
                    $it->url = '';
                } else if ($_POST["imageType"] === "deleteImage") {
                    $_SESSION["lien_image"] = '';
                    $it->img = 'noimage.jpg';
                    $it->url = '';
                }

                $it->save();
            }
            $_SESSION['mode_item'] = $_POST["valider"];
            $l = Item::where('id', '=', $_SESSION['item']->id)->first();
            $_SESSION['item'] = $l;
            $item = $l;

        }


        if (isset($_POST["creerCagnotte"])) {
            if (Cagnotte::where('iditem', '=', $item->id)->first() == null) {
                $cagnotte = new Cagnotte();
                $cagnotte->iditem = $_SESSION['item']->id;
                $cagnotte->tarifRestant = $_SESSION['item']->tarif;

                $cagnotte->save();
                $_SESSION["cagnotte"] = $cagnotte;
            }
        }

        if (isset($_POST["valeurCagnotte"]) && $_POST["valeurCagnotte"] != '') {
            $cagnotte = Cagnotte::where('iditem', '=', $_SESSION['item']->id)->first();
            $cagnotte->tarifRestant = $cagnotte->tarifRestant - $_POST["valeurCagnotte"];

            if ($cagnotte->tarifRestant >= 0)
                $cagnotte->save();
        }
        $_SESSION['page']='item';
        $affiche->ajout($item);
        $affiche->render();
    }
}