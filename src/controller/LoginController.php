<?php

namespace mywishlist\controller;

use mywishlist\models\User;

use mywishlist\vue\VueLogin as VueLogin;

class LoginController {
    /*
     *
     */
    public static function afficher() {
        $affiche = new VueLogin();
        $_SESSION['login_faut'] = "";
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['password'] = base64_encode($_POST['password']);
            $user = User::where('username', '=', $_SESSION['username'])->where('mdp', '=', $_SESSION['password'])->first();
            if ($user != null) {
                setcookie("iduser", $user->iduser, time() + 3600 * 1);
                setcookie("participation", $user->username, time() + 3600 * 1);
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            } else {
                $affiche->geterror();
            }
        }
        if (isset($_POST['signup_username']) && isset($_POST['signup_password'])) {
            $user = new User();
            $user->username = $_POST['signup_username'];
            $user->mdp = base64_encode($_POST['signup_password']);
            $user->save();
            $_SESSION['iduser'] = $user->iduser;
        }
        if (isset($_POST['modifier_username']) && isset($_POST['modifier_password'])) {
            setcookie("iduser", "", time() - 100);
            User::where('username', $_POST['modifier_username'])
                ->update(['mdp' => base64_encode($_POST['modifier_password'])]);

        }


        if ($_SESSION['page'] == 'login') {
            $affiche->login();
        }
        if ($_SESSION['page'] == 'modifiermdp') {
            $affiche->mdp();
        }
        if ($_SESSION['page'] == 'signup') {
            $affiche->signup();
        }

        $affiche->render();
    }
}