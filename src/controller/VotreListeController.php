<?php

namespace mywishlist\controller;

use mywishlist\models\Liste;
use mywishlist\vue\VueVotreListe;

class VotreListeController {
    public static function afficher() {
        $affiche = new VueVotreListe();

        if (isset($_POST['numero_liste'])) {
            $liste = Liste::where("no", "=", $_POST['numero_liste'])->first();
            $_SESSION['numero_liste'] = $_POST['numero_liste'];
            if ($liste->publique === 1)
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->no]));
            else if ($liste->publique === 0)
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->token]));
        }
        $affiche->getListe();
        $affiche->render();
    }
}