<?php

namespace mywishlist\controller;

use mywishlist\models\AssociationListeMessages;
use mywishlist\models\Cagnotte;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Message;
use mywishlist\models\Reservation;
use mywishlist\models\User;

class CompteController {
    /*
     *
     */
    public static function supprimer() {
        $listes = Liste::where('user_id', '=', $_COOKIE['iduser'])->get();
        foreach ($listes as $liste) {
            $associations = AssociationListeMessages::where('idliste', '=', $liste['no'])->get();
            foreach ($associations as $assoc) {
                Message::where('idmessage', '=', $assoc['idmessage'])->delete();
            }
            AssociationListeMessages::where('idliste', '=', $liste['no'])->delete();

            $items = Item::where('liste_id', '=', $liste['no'])->get();
            foreach ($items as $item) {
                Cagnotte::where('iditem', '=', $item['id'])->delete();
                if (date('Y-m-d', strtotime($liste['expiration'])) > date('Y-m-d')) {
                    Reservation::where('iditem', '=', $item['id'])->delete();
                }
                $file = 'images' . $item['img'];
                if (is_file($file)) {
                    unlink($file);
                }
            }
            Item::where('liste_id', '=', $liste['no'])->delete();

        }
        User::where('iduser', '=', $_COOKIE['iduser'])->delete();
        setcookie("iduser", "", time() - 100);
        setcookie("participation", "", time() - 100);
        \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));


    }
}