<?php

namespace mywishlist\controller;

use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\vue\VueListe as VueListe;
use mywishlist\models\Message;
use mywishlist\models\AssociationListeMessages;

class ListeController {
    /*
     *
     */
    public static function afficher($liste) {
        $affiche = new VueListe();


        if (isset($_POST['ajouter'])) {

            $message = new Message();

            $message->contenu = $_POST['ajouter'];

            if ($_POST['ajouter'] != '') {
                $message->save();

                $assoc = new AssociationListeMessages();

                $assoc->idliste = $liste->no;
                $assoc->idmessage = $message->idmessage;
                $assoc->save();

            }
            $_SESSION['liste'] = $liste;
            \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
        }

        if (isset($_POST['listState'])) {
            $l = Liste::where("no", "=", $liste->no)->first();
            if ($l != null) {
                if ($l->publique === 0) {
                    $l->publique = 1;
                    $l->save();
                } else if ($l->publique === 1) {
                    $l->publique = 0;
                    $l->save();
                }
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            }
        }
        if (isset($_POST['ownList']) && isset($_COOKIE["iduser"])) {
            $l = Liste::where("no", "=", $liste->no)->first();
            if ($l != null && $l->user_id === -1) {
                $l->user_id = $_COOKIE["iduser"];
                $l->save();
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            }
        }

        if (isset($_POST["editList"])) {
            $_SESSION['editing'] = true;
        }

        if (isset($_POST['valider'])) {
            if (isset($_POST['liste_titre']) && isset($_POST['liste_description']) && isset($_POST['liste_expiration'])) {
                $l = Liste::where("no", "=", $liste->no)->first();
                $l->titre = $_POST['liste_titre'];
                $l->description = $_POST['liste_description'];
                $l->expiration = $_POST['liste_expiration'];

                $l->save();
                unset($_SESSION['editing']);
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            }
        }

        if (isset($_POST["addItems"])) {
            $_SESSION['addItems'] = true;
        }

        if (isset($_POST['validerItem'])) {
            if ((isset($_POST['item_nom']) && isset($_POST['item_desc']) && isset($_POST['item_tarif'])) || ($_POST["freeItems"]) && $_POST["freeItems"] != -1) {
                if ($_POST["freeItems"] && $_POST["freeItems"] != -1) {
                    $id = $_POST["freeItems"];
                    $item = Item::where("id", "=", $id)->first();
                    $item->liste_id = $liste->no;
                } else {
                    $item = new Item();
                    $item->liste_id = $liste->no;
                    $item->nom = $_POST['item_nom'];
                    $item->descr = $_POST['item_desc'];
                    $item->tarif = $_POST['item_tarif'];
                    $item->img = 'noimage.jpg';
                    $item->url = '';
                }
                $item->save();
                unset($_SESSION['addItems']);
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('accueil'));
            }
        }
        $affiche->ajout($liste);
        $affiche->render();
    }
}