<?php

namespace mywishlist\controller;

use mywishlist\models\Liste;
use mywishlist\vue\VueIndex as VueIndex;

class IndexController {

    public static function afficher() {
        $affiche = new VueIndex();
        if (isset($_POST['numero_liste'])) {
            $liste = Liste::where("no", "=", $_POST['numero_liste'])->first();
            if ($liste->publique == 1) {
                $_SESSION['numero_liste'] = $_POST['numero_liste'];
                \Slim\Slim::getInstance()->response->redirect(\Slim\Slim::getInstance()->urlFor('liste', ['n' => $liste->no]));
            }
        }
        $affiche->getindex();
        $affiche->render();
    }
}