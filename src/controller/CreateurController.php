<?php

namespace mywishlist\controller;

use mywishlist\vue\VueCreateur;

class CreateurController {

    public static function afficher() {
        $affiche = new VueCreateur();
        $affiche->getCreateur();
        $affiche->render();
    }
}