<?php

namespace mywishlist\controller;

use mywishlist\vue\VueNouvelleListe as VueNouvelleListe;
use mywishlist\models\Liste as Liste;

class NouvelleListeController {
    /*
     *
     */
    public static function afficher() {
        $affiche = new VueNouvelleListe();

        if (isset($_POST["title"]) && isset($_POST["desc"]) && isset($_POST["exp"])) {
            $titre = $_POST["title"];
            $desc = $_POST["desc"];
            $exp = $_POST["exp"];

            try {
                $token = random_bytes(32);
                $token = bin2hex($token);

                $tokenModif = random_bytes(32);
                $tokenModif = bin2hex($tokenModif);

                $list = new Liste();

                $list->titre = $titre;
                $list->description = $desc;
                $list->expiration = $exp;
                $list->token = $token;
                $list->tokenModif = $tokenModif;

                if (isset($_SESSION['iduser']))
                    $iduser = $_SESSION['iduser'];
                else
                    $iduser = -1;


                $list->user_id = $iduser;

                if ($titre != '' && $desc != '' && $exp != '') {
                    $list->save();
                    $_SESSION["createdList"] = $list;
                }
            } catch (\Exception $e) {
            }
        }

        $affiche->ajout();
        $affiche->render();
    }
}