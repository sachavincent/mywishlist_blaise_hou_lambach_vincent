<?php

namespace mywishlist\models;

class Cagnotte extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'cagnotte';
    protected $primaryKey = 'idcagnotte';
    public $timestamps = false;

    public function item() {
        return $this->belongsTo('mywishlist\models\item', 'iditem');
    }
}

