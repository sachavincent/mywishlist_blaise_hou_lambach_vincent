<?php

namespace mywishlist\models;

class AssociationListeMessages extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'AssociationListeMessages';
    protected $primaryKey = ['idliste', 'idmessage'];
    public $incrementing = false;

    public $timestamps = false;

    public function message() {
        return $this->belongsTo('quizzbox\models\Message', 'idmessage');
    }

    public function liste() {
        return $this->belongsTo('quizzbox\models\Liste', 'idliste');
    }
}